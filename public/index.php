<?php
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.

header('Content-Type: text/html; charset=UTF-8');
function generate_login(){
    $chars = "qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP_-=/";
    $words = ['Mr', 'Dr', 'Cat', 'Dog', 'strong', 'spot', 'word', 'world', 'Sunday', 'Monday', 'tuesday', 'wednesday', 'thursday', 'Big', 'Small', 'Corn'];
    $size = StrLen($chars) - 1;
    $login = null;
    $login .= $words[rand(0, count($words) - 1)];
    $max = 4;
    while ($max--)
        $login .= $chars[rand(0, $size)];
    $login .= strval(rand(111, 999));
    return $login;
}
function generate_password(){
    $chars = "qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP_-=/";
    $words = ['Mr', 'Dr', 'Cat', 'Dog', 'strong', 'spot', 'word', 'world', 'Sunday', 'Monday', 'tuesday', 'wednesday', 'thursday', 'Big', 'Small', 'Corn'];
    $max = 10;
    $size = StrLen($chars) - 1;
    $pass = null;
    while ($max--)
        $pass .= $chars[rand(0, $size)];
   return $pass;
}
// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    // Массив для временного хранения сообщений пользователю.
    $messages = array();
    $messages['res_saved'] = '';
    $messages['can_login'] = '';
    if (!empty($_COOKIE['save'])) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('save', '', 100000);
        setcookie('login', '', 100000);
        setcookie('pass', '', 100000);
        // Если есть параметр save, то выводим сообщение пользователю.
        $messages['res_saved'] = 'Спасибо, результаты сохранены.';
        // Если в куках есть пароль, то выводим сообщение.
        if (!empty($_COOKIE['pass'])) {
            $messages['can_login'] = sprintf('<div class="logininfo">Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
          и паролем <strong>%s</strong> для изменения данных.</div>',
                strip_tags($_COOKIE['login']),
                strip_tags($_COOKIE['pass']));
        }
    }

    // Складываем признак ошибок в массив.
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['birthday'] = !empty($_COOKIE['birthday_error']);
    $errors['gender'] = !empty($_COOKIE['gender_error']);
    $errors['limbs'] = !empty($_COOKIE['limbs_error']);
    $errors['biography'] = !empty($_COOKIE['biography_error']);

    $errors['superskills'] = !empty($_COOKIE['superskills_error']);
    $errors['agreement'] = !empty($_COOKIE['agreement_error']);

    // Выдаем сообщения об ошибках.
    if ($errors['fio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '', 100000);
        // Выводим сообщение.
        $messages['fio'] = '<div class="error">Поле с именем не должно быть пустым.</div>';
    }
    if ($errors['email']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('email_error', '', 100000);
        // Выводим сообщение.
        $messages['email'] = '<div class="error">Заполните почту в формате email@example.com.</div>';
    }
    if ($errors['birthday']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('birthday_error', '', 100000);
        // Выводим сообщение.
        $messages['birthday'] = '<div class="error">Выберите дату.</div>';
    }
    if ($errors['gender']) {
         setcookie('sex_error', '', 100000);

        $messages['gender'] = '<div class="error">Выберите свой гендер.</div>';
    }
    if ($errors['limbs']) {
        setcookie('limbs_error', '', 100000);

        $messages['limbs'] = '<div class="error">Выберите количество конечностей.</div>';
    }
    if ($errors['biography']) {
        setcookie('biography_error', '', 100000);

        $messages['biography'] = '<div class="error">Поле с биографией не должно быть пустым.</div>';
    }
    if ($errors['superskills']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('superskills_error', '', 100000);
        // Выводим сообщение.
        $messages['superskills'] = '<div class="error">Должна быть выбрана хотя бы одна способность.</div>';
    }
    if ($errors['agreement']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('agreement_error', '', 100000);
        // Выводим сообщение.
        $messages['agreement'] = '<div class="error">Вы должны согласиться с условиями , прежде чем продолжить.</div>';
    }


    // Складываем предыдущие значения полей в массив, если есть.




    // Если нет предыдущих ошибок ввода, есть кука сессии, начали сессию и
    // ранее в сессию записан факт успешного логина.
    if (!empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
        // TODO: загрузить данные пользователя из БД
        // и заполнить переменную $values,
        // предварительно санитизовав.
        $dbuser = 'u21044';
        $dbpass = '76898768';
        $db = new PDO('mysql:host=localhost;dbname=u21044', $dbuser, $dbpass, array(PDO::ATTR_PERSISTENT => true));
        $stmt = $db->prepare("SELECT * FROM application WHERE id = ?");
        $stmt->execute([$_SESSION['uid']]);
        $row = $stmt ->fetch(PDO::FETCH_ASSOC);

        $values = array();
        $values['fio'] = $row["fio"];
        $values['email'] = $row["email"];
        $values['birthday'] = $row["birthdate"];
        $values['gender'] = $row["sex"];
        $values['limbs'] = $row["limbs"];
        $values['biography'] = $row["biography"];
        $stmt = $db->prepare("SELECT sup_id FROM app_sup WHERE id = ?");
        $stmt->execute([$_SESSION['uid']]);
        $superskills = $stmt->fetchAll(PDO::FETCH_COLUMN);
        $messages['login'] = '<div class="logininfo">Вход с логином <span class="imp">%s</span>, uid <span class="imp">%d</span></div>';
        $values['agreement'] = 'on';
    }
    else {
        $values = array();
        $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
        $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
        $values['birthday'] = empty($_COOKIE['birthday_value']) ? '' : $_COOKIE['birthday_value'];
        $values['gender'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
        $values['limbs'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
        $values['biography'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
        $values['agreement'] = empty($_COOKIE['agreement_value']) ? '' : $_COOKIE['agreement_value'];
        $superskills = array();
        $superskills = empty($_COOKIE['superskills_values']) ? array() : unserialize($_COOKIE['superskills_values'], ["allowed_classes" => false]);
    }

    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include('form.php');
}

// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
    // Проверяем ошибки.
    $errors = FALSE;
    if (empty($_POST['fio'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }
    if (!preg_match('/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+$/', $_POST['email'])) {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else{
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['birthday'])) {
        // Выдаем куку на день с флажком об ошибке в поле birthday.
        setcookie('birthday_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('birthday_value', $_POST['birthday'], time() + 30 * 24 * 60 * 60);
    }
    if (!isset($_POST['gender'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('gender_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('gender_value', $_POST['gender'], time() + 30 * 24 * 60 * 60);
    }
    if (!isset($_POST['limbs'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('limbs_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['biography'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('biography_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
    }

    if (!isset($_POST['superskills'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('superskills_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {

        setcookie('superskills_values', serialize($_POST['superskills']), time() + 30 * 24 * 60 * 60);
    }
    if (!isset($_POST['agreement'])) {
        setcookie('agreement_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else{
        setcookie('agreement_value', $_POST['agreement'], time() + 30 * 24 * 60 * 60);
    }

    // *************
    // Сохранить в Cookie признаки ошибок и значения полей.
    // *************




// *************
// Тут необходимо проверить правильность заполнения всех остальных полей.
// *************

    if ($errors) {
        // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
        header('Location: index.php');
        exit();
    }
    else {
        // Удаляем Cookies с признаками ошибок.
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('birthday_error', '', 100000);
        setcookie('gender_error', '', 100000);
        setcookie('limbs_error', '', 100000);
        setcookie('superskills_error', '', 100000);
        setcookie('biography_error', '', 100000);
        setcookie('agreement_error', '', 100000);
    }

// Сохранение в базу данных.
// Сохранение в XML-документ.



    $dbuser = 'u21044';
    $dbpass = '76898768';
    $db = new PDO('mysql:host=localhost;dbname=u21044', $dbuser, $dbpass, array(PDO::ATTR_PERSISTENT => true));

    if (!empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
        // TODO: перезаписать данные в БД новыми данными,
        // кроме логина и пароля.

        $stmt = $db->prepare("UPDATE application SET fio = ?, email = ?, birthdate = ?, sex = ? , limbs = ?, biography = ? WHERE id = ?");
        $stmt -> execute([$_POST['fio'],$_POST['email'],$_POST['birthday'],$_POST['gender'],$_POST['limbs'],$_POST['biography'],$_SESSION['uid']]);
        $stmt2 = $db->prepare("INSERT INTO app_sup SET id= ?, sup_id = ?");

        foreach ($_POST['superskills'] as $s)
            $stmt2 -> execute([$_SESSION['uid'], $s]);
    }
    else {
        // Генерируем уникальный логин и пароль.
        // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
        $login = generate_login();
        $pass = generate_password();
        // Сохраняем в Cookies.
        setcookie('login', $login);
        setcookie('pass', $pass);
        // TODO: Сохранение данных формы, логина и хеш md5() пароля в базу данных.
        // ...
        $stmt = $db->prepare("INSERT INTO application SET fio = ?, email = ?, birthdate = ?, sex = ? , limbs = ?, biography = ?");
        $stmt -> execute([$_POST['fio'],$_POST['email'],$_POST['birthday'],$_POST['gender'],$_POST['limbs'],$_POST['biography']]);
        $stmt2 = $db->prepare("INSERT INTO app_sup SET id= ?, sup_id = ?");
        $user_id = $db->lastInsertId();
        foreach ($_POST['superskills'] as $s)
            $stmt2 -> execute([$user_id, $s]);

        $stmt = $db->prepare("INSERT INTO users SET login = ?, passhash = ?, id = ?");
        $stmt -> execute([$login,password_hash($pass, PASSWORD_DEFAULT),$user_id]);
    }

// Сохраняем куку с признаком успешного сохранения.
    setcookie('save', '1');

    header('Location: ?save=1');
}
