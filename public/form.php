<!DOCTYPE html>
<html lang="ru">
<link rel="stylesheet" href="styl.css">
<body>
<?php
if (!empty($messages)) {
    print $messages['can_login'];
    if(!empty($_SESSION['login'])){
        printf($messages['login'], $_SESSION['login'], $_SESSION['uid']);
        echo '<a href="login.php?exit=1">Выход</a>';}
    else echo '<a href="login.php">Вход</a>';
}
?>
     <form class="railway" action="" method="POST">
            <div class="stripes-block">
            <div class="line"></div>
            </div>
                <?php if (!empty($messages)){
                    echo '
         <div class="forminfo">
                    ';
                    print $messages['res_saved'];
                    echo '
         </div>
                    ';
                }
                    ?>
            <div class="form-group">
            <label>
                Имя<br />
                <input name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>"/>
            </label><br />
            <?php if ($errors['fio']) {print $messages['fio'];} ?>

            <label>
                E-mail:<br />
                <input name="email"
                    <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"
                       type="email" />
            </label><br />
            <?php if ($errors['email']) {print $messages['email'];} ?>

            <label>
                Дата рождения:<br />
                <input name="birthday"
                    <?php if ($errors['birthday']) {print 'class="error"';} ?> value="<?php print $values['birthday']; ?>"
                       type="date" min="1900-01-01" max="2022-12-31">
            </label><br />
            <?php if ($errors['birthday']) {print $messages['birthday'];} ?>
            <label>Пол:</label><br />
            <?php if ($errors['gender']) {print '<div class="error">';} ?>
            <label><input type="radio"
                          name="gender" value="1" <?php if($values['gender']=='1') print "checked";else print ""; ?> />
                мужской
            </label>
            <label><input type="radio"
                          name="gender" value="2" <?php echo ($values['gender']=='2') ?  "checked" : "" ;  ?> />
                женский
            </label>
            <label><input type="radio"
                          name="gender" value="3" <?php echo ($values['gender']=='3') ?  "checked" : "" ;  ?> />
                другое
            </label>
            <label><input type="radio"
                          name="gender" value="4" <?php echo ($values['gender']=='4') ?  "checked" : "" ;  ?> />
                не знаю
            </label>
            <label><input type="radio"
                          name="gender" value="5" <?php echo ($values['gender']=='5') ?  "checked" : "" ;  ?> />
                 лава
            </label>
            <?php if ($errors['gender']) {print '</div>';print $messages['gender'];} ?>
            <div class="limbs">

            <label>Количество конечностей:</label><br />
            <?php if ($errors['limbs']) {print '<div class="error">';} ?>
            <label><input type="radio"
                          name="limbs" value="0" <?php echo ($values['limbs']=='0') ?  "checked" : "" ;  ?>/>
                0
            </label>
            <label><input type="radio"
                          name="limbs" value="1" <?php echo ($values['limbs']=='1') ?  "checked" : "" ;  ?>/>
                1
            </label>
            <label><input type="radio"
                          name="limbs" value="2" <?php echo ($values['limbs']=='2') ?  "checked" : "" ;  ?>/>
                2
            </label>
            <label><input type="radio"
                          name="limbs" value="3" <?php echo ($values['limbs']=='3') ?  "checked" : "" ;  ?>/>
                3
            </label>
            <label><input type="radio"
                          name="limbs" value="4" <?php echo ($values['limbs']=='4') ?  "checked" : "" ;  ?>/>
                4
            </label>
            <label><input type="radio"
                          name="limbs" value="5" <?php echo ($values['limbs']=='5') ?  "checked" : "" ;  ?>/>
                5+
            </label>
            </div>
            <?php if ($errors['limbs']) {print '</div>';print $messages['limbs'];} ?>
            <?php echo (in_array("0",$superskills)) ?  "selected" : ""  ; ?>
            <label>
                Сверхспособности:
                <br />
                <select name="superskills[]"
                        multiple="multiple" <?php if ($errors['superskills']) {print 'class="error"';} ?>>
                    <option value="1" <?php echo (in_array('1',$superskills)) ?  "selected" : ""  ; ?>>быстрый нагрев тела до 1000000&ordm;C</option>
                    <option value="2" <?php echo (in_array('2',$superskills)) ?  "selected" : ""  ; ?>>испускание света из ладоней</option>
                    <option value="3" <?php echo (in_array('3',$superskills)) ?  "selected" : ""  ; ?>>дыхание под водой</option>
                    <option value="3" <?php echo (in_array('4',$superskills)) ?  "selected" : ""  ; ?>>рабивать стекло голосом</option>
                </select>
            </label><br />
            <?php if ($errors['superskills']) {print $messages['superskills'];} ?>

            <label>
                Биография:<br />
                <textarea name="biography" <?php if ($errors['biography']) {print 'class="error"';} ?>><?php echo $values['biography'] ;  ?></textarea>
            </label><br />
            <?php if ($errors['biography']) {print $messages['biography'];} ?>
            <br />
            <label class="checkbox">c контрактом ознакомлен<input type="checkbox"
                          name="agreement" <?php if ($errors['agreement']) {print 'class="error"';} echo ($values['agreement']=='on') ? 'checked': ''; ?>/>

            </label><br />
            <?php if ($errors['agreement']) {print $messages['agreement'];} ?>

            <input id="submit" type="submit" value="ok" />
            </div>
        </form>
</body>

</html>
